data "template_file" "cloud_config" {
  template = "${file("${path.module}/cloud-config.yml")}"
  vars = {
    hostname = "${var.hostname}"
    nfs_instance = "${var.nfs}"
    phone_home_url = "${var.phone_home_url}"
  }
}

resource "aws_instance" "swarm-host" {
  count                 = "${var.instance_count}"
  ami                   = "${lookup(var.ami, var.region)}"
  instance_type         = "${var.instance_type}"
  iam_instance_profile  = "${var.iam_role}"
  key_name              = "${var.ssh_key}"
  tags = {
      Name              = "${var.hostname}-${count.index + 1}"
  }
  root_block_device {
    volume_size         = "${var.volume_size}"
    volume_type         = "${var.disk_type}"
  }
  user_data = "${data.template_file.cloud_config.rendered}"
}

data "aws_route53_zone" "public" {
  name = "${var.public_dns_zone}"
}

resource "aws_route53_record" "swarm_host_public" {
  count   = "${var.instance_count}"
  zone_id = "${data.aws_route53_zone.public.zone_id}"
  name    = "${aws_instance.swarm-host.*.id[count.index]}"
  type    = "A"
  ttl     = "60"
  records = ["${aws_instance.swarm-host.*.public_ip[count.index]}"]
}

data "aws_route53_zone" "private" {
  name         = "${var.private_dns_zone}"
  private_zone = true
}

resource "aws_route53_record" "swarm_host_private" {
  count   = "${var.instance_count}"
  zone_id = "${data.aws_route53_zone.private.zone_id}"
  name    = "${aws_instance.swarm-host.*.id[count.index]}.${data.aws_route53_zone.private.name}"
  type    = "A"
  ttl     = "60"
  records = ["${aws_instance.swarm-host.*.private_ip[count.index]}"]
}

resource "aws_route53_record" "swarm_rr" {
  count   = "${var.instance_count}"
  zone_id = "${data.aws_route53_zone.private.zone_id}"
  name    = "${var.rr_dns}"
  type    = "CNAME"
  ttl     = "60"
  set_identifier = "${aws_instance.swarm-host.*.id[count.index]}"
  records        = ["${aws_route53_record.swarm_host_private.*.fqdn[count.index]}"]

  weighted_routing_policy {
    weight = 10
  }
}
