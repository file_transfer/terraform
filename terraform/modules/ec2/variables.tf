variable "hostname" {}

variable "region" {}

variable "ami" {
  type = "map"
  default = {
    us-west-2 = "ami-0816245342fc4a87a"
  }
}

variable "instance_count" {
  default = "2"
}


variable "instance_type" {
  default = "t2.large"
}

variable "iam_role" {
  default = "ec2-iam"
}

variable "ssh_key" {
  default = "eflores"
}

variable "volume_size" {
  default = "40"
}

variable "disk_type" {
  default = "gp2"
}

variable "nfs" {
  default = "fs-5fcff1f7.efs.us-west-2.amazonaws.com"
}

variable "phone_home_url" {
  default = "http://registry.flores.internal:5001/instance"
}

variable "public_dns_zone" {
  default = "flores.io."
}

variable "private_dns_zone" {
  default = "flores.internal."
}

variable "rr_dns" {
  default = "swarm" 
}
