terraform {
  backend "s3" {
    bucket = "eflores-terraform"
    key    = "swarm"
    region = "us-west-2"
  }
}

provider "aws" {
  region = "${var.region}"
}

