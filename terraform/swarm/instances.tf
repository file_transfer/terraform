variable "region" {
  type = "string"
  default = "us-west-2"
}
module "swarm-01" {
  source = "../modules/ec2"
  
  hostname = "swarm"
  region = "${var.region}"
  instance_count = "1"
}

