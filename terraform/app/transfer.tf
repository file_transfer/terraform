terraform {
  backend "s3" {
    bucket = "eflores-terraform"
    key    = "app"
    region = "us-west-2"
  }
}

variable "swarm" {
  default = "swarm.flores.internal"
}

variable "nats" {
  default = "transfer_nats"
}

provider "docker" {
  host = "tcp://${var.swarm}:2375/"
  version = "~> 2.0"
}

resource "docker_network" "transfer_net" {
  name   = "transfer_net"
  driver = "overlay"
  attachable = true
}

resource "docker_service" "nats" {
  name = "${var.nats}"
  
  task_spec {
    container_spec {
      hostname = "nats"
      image = "registry.flores.internal:5000/nats:2.0.0"
      args = ["-c", "/nats.conf", "-DV"]
    }
    networks     = ["${docker_network.transfer_net.id}"]
  }
}

resource "docker_service" "dispatcher" {
  name = "transfer_dispatcher"

  task_spec {
    container_spec {
      hostname = "dispatcher"
      image = "registry.flores.internal:5000/dispatcher:0.9.7"
      user  = "node"

      env = {
        MSG_HOST    = "${var.nats}"
        UPLOAD_DIR  = "/nfs/upload"
        LOG_FILE    = "/nfs/dispatcher.log"
        LOG_LEVEL   = "debug"
        MSG_USER    = "dispatcher"
        MSG_PASS    = "bar"
      }

      mounts {
          target    = "/nfs"
          source    = "/nfs/files"
          type      = "bind"
      }
    }
    networks     = ["${docker_network.transfer_net.id}"]
  }
}

variable "aws_id" {}
variable "aws_key" {}

resource "docker_secret" "aws_credentials" {
  name = "${uuid()}"
  data = "${base64encode("[default]\naws_access_key_id=${var.aws_id}\naws_secret_access_key=${var.aws_key}")}"
}

resource "docker_service" "worker" {
  name = "transfer_worker"

  task_spec {
    container_spec {
      hostname = "worker"
      image = "registry.flores.internal:5000/worker:0.9.4"
      user  = "worker"

      env = {
        MSG_HOST      = "${var.nats}"
        UPLOAD_DIR    = "/nfs/upload"
        LOG_FILE      = "/nfs/dispatcher.log"
        LOG_LEVEL     = "debug"
        MSG_USER      = "worker"
        MSG_PASS      = "foo"
        AWS_REGION    = "us-west-2"
        UPLOAD_BUCKET = "eflores-file-test"
      }

      secrets {
        secret_id = "${docker_secret.aws_credentials.id}"
        secret_name = "${docker_secret.aws_credentials.name}"
        file_name = "/home/worker/.aws/credentials"
      }

      mounts {
          target    = "/nfs"
          source    = "/nfs/files"
          type      = "bind"
      }
    }
    networks     = ["${docker_network.transfer_net.id}"]
  }
}
