#!/bin/bash
cd /root
git clone https://gitlab.com/file_transfer/devops-api.git 
docker-compose -f ./gitlab.yml up -d
docker-compose -f ./api.yml up -d
