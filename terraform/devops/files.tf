variable "slack_webhook" {}

data "template_file" "api-compose" {
  template = "${file("${path.module}/files/api.yml")}"
  vars = {
    slack_webhook = "${var.slack_webhook}"
  }
}

variable "gitlab_runner_token" {}

data "template_file" "runner-config" {
  template = "${file("${path.module}/files/config.toml")}"
  vars = {
    gitlab_runner_token = "${var.gitlab_runner_token}"
  }
}

data "template_file" "cloud_config" {
  template = "${file("${path.module}/files/cloud-config.yml")}"
  vars = {
    hostname = "${var.hostname}"
    nfs_instance = "${var.nfs}"
    gitlab_compose = "${base64encode(file("${path.module}/files/devops.yml"))}"
    run_containers = "${base64encode(file("${path.module}/files/run-containers.sh"))}"
    api_compose = "${base64encode("${data.template_file.api-compose.rendered}")}"
    runner_config = "${base64encode("${data.template_file.runner-config.rendered}")}"
  }
}
