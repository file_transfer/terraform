
data "aws_route53_zone" "private" {
  name         = "${var.private_dns_zone}"
  private_zone = true
}

resource "aws_route53_record" "registry" {
  zone_id = "${data.aws_route53_zone.private.zone_id}"
  name    = "registry"
  type    = "A"
  ttl     = "60"
  records = ["${aws_instance.devops-01.private_ip}"]
}

data "aws_route53_zone" "public" {
  name         = "${var.public_dns_zone}"
}

resource "aws_route53_record" "devops" {
  zone_id = "${data.aws_route53_zone.public.zone_id}"
  name    = "devops"
  type    = "A"
  ttl     = "60"
  records = ["${aws_instance.devops-01.public_ip}"]
}
