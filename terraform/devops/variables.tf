
variable "region" {
  default = "us-west-2"
}
variable "hostname" {
  default = "devops-01"
}

variable "nfs" {
  default = "fs-5fcff1f7.efs.us-west-2.amazonaws.com"
}

variable "instance_type" {
  default = "t2.large"
}

variable "iam_profile" {
  default = "ec2-iam"
}

variable "ssh_key" {
  default = "eflores"
}

variable "ami" {
  default = "ami-0816245342fc4a87a"
}

variable "disk_size" {
  default = "40"
}

variable "disk_type" {
  default = "gp2"
}


variable "private_dns_zone" {
  default = "flores.internal."
}

variable "public_dns_zone" {
  default = "flores.io."
}