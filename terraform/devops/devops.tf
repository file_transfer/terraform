# Sets up the devops host
terraform {
  backend "s3" {
    bucket = "eflores-terraform"
    key    = "devops"
    region = "us-west-2"
  }
}

provider "aws" {
  region = "${var.region}"
  version = "~> 2.16.0"
}

resource "aws_instance" "devops-01" {
  ami                   = "${var.ami}"
  instance_type         = "${var.instance_type}"
  iam_instance_profile  = "${var.iam_profile}"
  key_name              = "${var.ssh_key}"
  tags = {
      Name              = "${var.hostname}"
    }
  root_block_device {
    volume_size         = "${var.disk_size}"
    volume_type         = "${var.disk_type}"
  }
  user_data = "${data.template_file.cloud_config.rendered}"
}
