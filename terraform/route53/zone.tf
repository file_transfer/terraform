terraform {
  backend "s3" {
    bucket = "eflores-terraform"
    key    = "route53"
    region = "us-west-2"
  }
}

provider "aws" {
  region = "${var.region}"
}

variable "region" {
  type = "string"
  default = "us-west-2"
}

# Default VPC in OR
variable "vpc_id" {
  type = "string"
  default = "vpc-05719062"
}

variable "zone" {
  type = "string"
  default = "flores.internal."
}

resource "aws_route53_zone" "private" {
  name = "${var.zone}"
  force_destroy = true

  vpc {
    vpc_id = "${var.vpc_id}"
  }
}