# Sets up the devops host
terraform {
  backend "s3" {
    bucket = "eflores-terraform"
    key    = "ami"
    region = "us-west-2"
  }
}

provider "aws" {
  region = "${var.region}"
  version = "~> 2.16.0"
}

variable "region" {
  default = "us-west-2"
}


resource "aws_instance" "ami" {
  ami                   = "ami-005bdb005fb00e791"
  instance_type         = "t2.large"
  iam_instance_profile  = "ec2-iam"
  key_name              = "eflores"
  tags = {
      Name              = "ami-template"
    }
  root_block_device {
    volume_size         = "8"
    volume_type         = "gp2"
  }
  user_data = "${base64encode(file("${path.module}/cloud-config.yml"))}"
}

output "ssh" {
  value = "ssh ubuntu@${aws_instance.ami.public_ip}"
}
