#!/bin/sh
set -e
cd terraform
for DIR in $1; do
  cd $DIR
  echo "Testing terraform dir '$DIR'"
  set -x
  terraform init
  terraform validate
  terraform plan
  { set +x; } 2>/dev/null
  cd ..
done
