#!/bin/sh
REPO_URL="<${CI_PROJECT_URL}|${CI_PROJECT_PATH}>"
PIPELINE_URL="<https://gitlab.com/${CI_PROJECT_PATH}/pipelines/${CI_PIPELINE_ID}|#${CI_PIPELINE_ID}>"

curl -s \
  -d "{\"text\": \"${REPO_URL}: Pipeline ${PIPELINE_URL} of branch \`${CI_COMMIT_REF_NAME}\` status: \n$1\" }" \
  $GIT_WEBHOOK