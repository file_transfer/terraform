#!/bin/sh
set -e

DIR=$1
ACTION=$2

if [[ $ACTION = "up" ]]; then
  COMMAND="terraform apply -auto-approve -no-color"
elif [[ $ACTION = "down" ]]; then
  COMMAND="terraform destroy -auto-approve -no-color"
elif [[ $ACTION = "plan" ]]; then
  COMMAND="terraform plan -no-color"
else
  echo "Invalid action \"${ACTION}\"" 1>&2
  exit 1
fi
echo $COMMAND

./scripts/webhook_msg.sh "Starting chatops stage, command - \`${COMMAND}\`"

cd terraform/$DIR

terraform init

echo -e "\n$COMMAND"

RESULT=$(${COMMAND})

echo -e "section_start:$( date +%s ):chat_reply\r\033[0K\n$(echo "\n$RESULT")\nsection_end:$( date +%s ):chat_reply\r\033[0K"
